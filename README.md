### Description

This is a simple Node.js and Python script for Broadlink SP2 Smart Plug
It will set up a web server on port 3000 and will listen to GET requests
It will send the request to the Python script and using the python-broadlink will turn on/off the SP2 smart plug.

This script is based on https://github.com/NightRang3r/Broadlink-NodeSP2, but adjusted to work from docker.


### Installation
For the docker image see: https://bitbucket.org/mgeisinger/dockerlink-nodesp2